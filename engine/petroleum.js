var input = document.getElementById('input');
var output = document.getElementById('output');

function minifyHTML() {
  output.value = input.value;
  output.value = output.value.replace(/\s/g, '').replace(/\/>/g, '>').replace(/\"/g, '\'').replace(/\src="https/g, 'src="').replace(/\href="https/g, 'href="').trim().replace(/\<!DOCTYPE/g, '<!DOCTYPE ').replace(/\<html/g, '<html ').replace(/\<meta/g, '<meta ').replace(/\<title/g, '<title ').replace(/\<link/g, '<link ').replace(/\<body/g, '<body ').replace(/\<div/g, '<div ').replace(/\<textarea/g, '<textarea ').replace(/\<button/g, '<button ').replace(/\<input/g, '<input ').replace(/\<iframe/g, '<iframe ').replace(/\<img/g, '<img ')
}

function minifyCSS() {
  output.value = input.value;
  output.value = output.value.replace(/\s/g, '').trim();
}

function minifyJS() {
  output.value = input.value;
  output.value = output.value.replace(/\s/g, '').replace(/\"/g, '\'').replace(/\;/g, '').trim();
}